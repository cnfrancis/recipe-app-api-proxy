#!/bin/sh

set -e # return error to screen if script fails somewhere, to have all commands run succesfully

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'
